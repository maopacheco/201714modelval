FROM spiralout/docker-apache-php-composer
MAINTAINER mauricio.pacheco@jtc.com.co
RUN apt-get update -y
RUN apt-get install git -y
RUN git config --global user.name "maopacheco"
RUN git config --global user.email mauricio.pacheco@jtc.com.co
RUN git clone https://gitlab.com/maopacheco/201714modelval.git		      
RUN mv 201714modelval /var/www/site/public
EXPOSE 80