# Ejercicio en Docker


## Instrucciones para acceder al servicio de suma

1. Por medio del siguiente comando se realiza una copia del repositorio en gitlab al local:
>
git clone https://gitlab.com/maopacheco/201714modelval.git
>

2. Una vez se tenga el repositorio en el local, se ejecuta el siguiente comando para correr la imagen que se encuentra den docker HUB:
>
sh start.sh
>

3. Luego en el navegador ingresa por la siguiente URL:
>
http://localhost/201714modelval/suma.php
>

4. Seguido de suma.php agregamos el simbolo "?" y se realizan la suma de las 2 variables por medio del servicio:
>
Ej: http://localhost/201714modelval/suma.php?n1=5&n2=32
>

5. Para detener el servicio ejecutamos el siguiente comando en la terminal:
>
sh stop.sh
>